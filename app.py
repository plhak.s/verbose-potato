import os
import sys
import http.server
import socketserver
import subprocess


class httpRequestHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        if self.path == "/date":
            self.path = "static" + self.path + ".html"
        if self.path == "/time":
            if is_24:
                path = "/time"
            else:
                path = "/time_12"
            self.path = "static" + path + ".html"
        elif self.path == "/":
            self.path = "static/index.html"
        return http.server.SimpleHTTPRequestHandler.do_GET(self)


conf = subprocess.run(["grep", "port[[:space:]]*=[[:space:]]*", ".env"], stdout=subprocess.PIPE)
if b"=" not in conf.stdout:
    print("Could not read configuration file `/var/tmp/mytime/mytime.conf`")
    sys.exit(1)
port = conf.stdout.split(b"=")[1]

conf = subprocess.run(["grep", "format[[:space:]]*=[[:space:]]*", ".env"], stdout=subprocess.PIPE)
if b"=" in conf.stdout:
    time_format = conf.stdout.split(b"=")[1]
    is_24 = b"24h" in time_format
else:
    is_24 = True

socketserver.TCPServer.allow_reuse_address = True
server = socketserver.TCPServer(("", int(port)), httpRequestHandler)
server.serve_forever()
